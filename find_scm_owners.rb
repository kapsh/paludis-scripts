#! /usr/bin/ruby

require 'Paludis'

def die(msg)
    $stderr.puts "#$0: #{msg}"
    exit 1
end

Paludis::Log.instance.program_name = $0
$envspec = ""

require 'getoptlong'
GetoptLong.new(

    ["--log-level",           GetoptLong::REQUIRED_ARGUMENT],
    ["--environment",   "-E", GetoptLong::REQUIRED_ARGUMENT]

).each do | opt, arg |
    case opt

    when "--log-level"
        Paludis::Log.instance.log_level = case arg
            when "debug"
                Paludis::LogLevel::Debug
            when "qa"
                Paludis::LogLevel::Qa
            when "warning"
                Paludis::LogLevel::Warning
            when "silent"
                Paludis::LogLevel::Silent
            else
                die "invalid #{opt}: #{arg}"
        end
    when "--environment"
        $envspec = arg

    end
end

ARGV.empty? or die "no command-line arguments supported"

$env = Paludis::EnvironmentFactory.instance.create($envspec)
inst = $env[Paludis::Selection::AllVersionsUnsorted.new(Paludis::Generator::All.new | Paludis::Filter::InstalledAtRoot.new($env.preferred_root_key.parse_value))]

inst.each do | pkg |
    key = pkg["INHERITED"] or next
    (value = key.parse_value).kind_of?(Array) or next
    value.include?("scm") or next
    choices = pkg.choices_key and choices = choices.parse_value

    repo = $env.fetch_repository(pkg.repository_name)
    repo = repo.environment_variable_interface or next

    active = true
    level  = 0
    checkout_vars = repo.get_environment_variable(pkg, "SCM_SECONDARY_REPOSITORIES").split.collect do | t |
        case t

            when /\?$/
                if active then
                    choice = choices.find_by_name_with_prefix(t.chomp("?"))
                    active = false unless choice && choice.enabled?
                end unless choices.nil?
                []

            when "("
                level += 1 unless active
                []

            when ")"
                if !active then
                    level -= 1
                    active = true if level == 0
                end
                []

            else
                active ? ["SCM_#{t}_CHECKOUT_TO"] : []

        end
    end.flatten

    repo.get_environment_variable(pkg, "SCM_NO_PRIMARY_REPOSITORY").empty? and checkout_vars << "SCM_CHECKOUT_TO"

    checkouts = []
    checkout_vars.each do | var |
        checkout = repo.get_environment_variable(pkg, var)
        checkouts << checkout unless checkout.empty?
    end

    if !checkouts.empty? then
        puts "#{pkg}:"
        checkouts.each { | co | puts "    #{co}" }
    end
end

