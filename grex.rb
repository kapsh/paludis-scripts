#!/usr/bin/ruby -w
# vim: set sw=4 sts=4 et tw=80 ft=ruby :
# $Id$

# Copyright (c) 2007 Mike Kelly <pioto@pioto.org>
#
# This file is part of the Paludis package manager. Paludis is free software;
# you can redistribute it and/or modify it under the terms of the GNU General
# Public License, version 2, as published by the Free Software Foundation.
#
# Paludis is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307  USA

%w(Paludis getoptlong rexml/document net/http uri fileutils).each {|x| require x}

include Paludis

Log.instance.log_level = Paludis::LogLevel::Warning
Log.instance.program_name = $0

version = "0.1.1"
herdsxml_url = URI.parse('http://sources.gentoo.org/viewcvs.py/*checkout*/gentoo/xml/htdocs/proj/en/metastructure/herds/herds.xml?content-type=text%2Fplain')

opts = GetoptLong.new(
    [ '--help',          '-h', GetoptLong::NO_ARGUMENT ],
    [ '--version',       '-V', GetoptLong::NO_ARGUMENT ],
    [ '--log-level',           GetoptLong::REQUIRED_ARGUMENT ],
    [ '--environment',   '-E', GetoptLong::REQUIRED_ARGUMENT ],
    [ '--herd',                GetoptLong::NO_ARGUMENT ],
    [ '--package',             GetoptLong::NO_ARGUMENT ],
    [ '--dev',                 GetoptLong::NO_ARGUMENT ],
    [ '--verbose',       '-v', GetoptLong::NO_ARGUMENT ])

env_spec = ''
verbose = false
mode = 'package'

opts.each do | opt, arg |
    case opt
    when '--help'
        puts "Usage: " + $0 + " [options] <name> [<name> ...]"
        puts
        puts "Options:"
        puts "  -h|--help                  Display a help message"
        puts "  -V|--version               Display program version"
        puts "  --log-level level          Set log level (debug, qa, warning, slient)"
        puts "  -E|--environment env       Environment specification (class:suffix, both parts optional)"
        puts
        puts "The following options affect what sort of information is shown:"
        puts "  --herd                     Show more information about the herds given."
        puts "  --package                  Show the maintainer data for the given package names (default)."
        puts "  --dev                      Show the herds that the given developer is a member of."
        puts
        puts "  -v|--verbose               Verbose output (e.g. lookup herd members)"
        exit 0

    when '--version'
        puts $0.to_s.split(/\//).last + " " + version + " (Paludis Version: " +Paludis::Version + ")"
        exit 0

    when '--log-level'
        case arg
        when 'debug'
            Paludis::Log.instance.log_level = Paludis::LogLevel::Debug
        when 'qa'
            Paludis::Log.instance.log_level = Paludis::LogLevel::Qa
        when 'warning'
            Paludis::Log.instance.log_level = Paludis::LogLevel::Warning
        when 'silent'
            Paludis::Log.instance.log_level = Paludis::LogLevel::Silent
        else
            puts "Bad --log-level value " + arg
            exit 1
        end
    when '--environment'
        env_spec = arg

    when '--herd'
        mode = "herd"
    when '--package'
        mode = "package"
    when '--dev'
        mode = "dev"

    when '--verbose'
        verbose = true

    end

end

env = EnvironmentFactory.instance.create(env_spec)

if ARGV.empty?
    puts "No arguments supplied"
    exit 1
end

package_db = env.package_database()

# build a hash of repo.name->repo.location
locations = Hash.new

package_db.repositories.each do | repo |
    next if repo.e_interface == nil
    repo.each_metadata do | key |
       locations[repo.name] = key.value if key.raw_name == 'location'
    end
end

cache_dir = File.expand_path '~/.grex'
herds_cache_file = "#{cache_dir}/herds.xml"

if File.readable?(herds_cache_file) && File.mtime(herds_cache_file) > Time.now - 24 * 60 * 60
    herdsxml = REXML::Document.new File.read(herds_cache_file)
else
    begin
        req = Net::HTTP::Get.new(herdsxml_url.path)
        res = Net::HTTP.start(herdsxml_url.host, herdsxml_url.port) {|http|
            http.request(req)
        }
        body =  res.body
    rescue
        puts "Couldn't fetch herds.xml."
        verbose = false
    end
    FileUtils.mkdir(cache_dir) unless File.directory? cache_dir
    File.open(herds_cache_file, 'w') {|x| x.print body}
    herdsxml = REXML::Document.new body
end

ARGV.each do | name |
    if mode == "package"
        package_name = name
        if package_name.include? ?/
            fqpn = QualifiedPackageName.new(package_name)
        else
            fqpn = package_db.fetch_unique_qualified_package_name(package_name)
        end
        puts "#{fqpn}"

        package_db.repositories.each do | repo |
            next if repo.e_interface == nil
            next unless repo.has_package_named? fqpn

            location = locations[repo.name]

            begin
                file = File.new(location + "/" + fqpn + "/metadata.xml")
            rescue Errno::ENOENT
                puts "  No metadata.xml for this repo (#{repo.name}). Skipping."
                next
            end
            puts "  #{repo.name}: #{location}"

            begin
                metadata = REXML::Document.new file
            rescue
                puts "  Error parsing metadata.xml for this repo (#{repo.name}). Skipping."
                next
            end

            metadata.elements.each("pkgmetadata/herd") do | element |
                herd = element.text.to_s
                if herd.empty?
                    Paludis::Log.instance.message("grex.empty_herd", Paludis::LogLevel::Qa, "Empty <herd> tag in metadata.xml for #{fqpn} in #{repo.name}.")
                    next
                end

                print "    herd:  #{herd}"

                if verbose
                    herdsxml.elements.each("herds/herd") do | element |
                        next unless element.text("name").strip == herd
                        print " <#{element.text("email").strip}>" unless element.text("email") == nil
                        puts
                        element.elements.each() do | maintainer |
                            name = maintainer.text("name").to_s.strip
                            email = maintainer.text("email").to_s.strip
                            next if name.empty? and email.empty?

                            print "      maintainer:  "

                            if email.empty?
                                print name
                            elsif name.empty?
                                print "<#{email}>"
                            else
                                print "#{name} <#{email}>"
                            end
                            print "\n"
                        end
                    end
                end
            end

            metadata.elements.each("pkgmetadata/maintainer") do | element |
                name = element.text("name").to_s.strip
                email = element.text("email").to_s.strip
                next if name.empty? and email.empty?

                print "    maintainer:  "

                if email.empty?
                    print name
                elsif name.empty?
                    print "<#{email}>"
                else
                    print "#{name} <#{email}>"
                end
                print "\n"
            end
        end
    elsif mode == "herd"
        print "#{name}"
        herdsxml.elements.each("herds/herd") do | element |
            element.context={:compress_whitespace => :all}
            next unless element.text("name").strip == name
            print " <#{element.text("email").strip}>" unless element.text("email") == nil
            puts

            desc = element.text("description").to_s.strip
            puts " :: #{desc}" unless desc.empty?

            proj_page = element.text("maintainingproject").to_s.strip
            puts " :: http://www.gentoo.org#{proj_page}" unless proj_page.empty?

            element.elements.each() do | maintainer |
                name = maintainer.text("name").to_s.strip
                email = maintainer.text("email").to_s.strip
                role = maintainer.text("role").to_s.strip
                next if name.empty? and email.empty?

                print "  maintainer:  "

                if email.empty?
                    print name
                elsif name.empty?
                    print "<#{email}>"
                else
                    print "#{name} <#{email}>"
                end
                print " (#{role})" unless role.empty?
                print "\n"
            end
        end
    end
end
